#!/bin/bash
cd  $OPENSHIFT_PRIMARY_CARTRIDGE_DIR
export HOME=$OPENSHIFT_PRIMARY_CARTRIDGE_DIR
nohup nodejs/bin/node sinopia/lib/cli.js &> $OPENSHIFT_PRIMARY_CARTRIDGE_DIR/sinopia.log &
